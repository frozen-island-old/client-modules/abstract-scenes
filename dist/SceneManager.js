/**
 * Provides scenes to the engine.
 */
export class SceneManager {
    /**
     * Nothing should need to go here...
     */
    constructor() { }
}
