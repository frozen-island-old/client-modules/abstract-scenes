/// <reference types="socket.io-client" />
import { SceneType } from "./SceneType";
import { SceneOnlineBase } from "./SceneOnlineBase";
/**
 * Minigame Class.
 */
export declare abstract class Minigame extends SceneOnlineBase {
    /**
     * The constructor for the Minigame class.
     * @param gameElement The element the game is running in.
     * @param socket The socket pointing to the server.
     */
    constructor(gameElement: HTMLDivElement, socket: SocketIOClient.Socket);
    protected __type(): SceneType;
}
