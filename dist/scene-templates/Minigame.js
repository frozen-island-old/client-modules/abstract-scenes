import { SceneType } from "./SceneType";
import { SceneOnlineBase } from "./SceneOnlineBase";
/**
 * Minigame Class.
 */
export class Minigame extends SceneOnlineBase {
    /**
     * The constructor for the Minigame class.
     * @param gameElement The element the game is running in.
     * @param socket The socket pointing to the server.
     */
    constructor(gameElement, socket) {
        super(gameElement, socket);
    }
    __type() {
        return SceneType.Minigame;
    }
}
;
