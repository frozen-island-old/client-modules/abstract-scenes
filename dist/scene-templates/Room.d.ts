/// <reference types="socket.io-client" />
import { SceneType } from "./SceneType";
import { SceneOnlineBase } from "./SceneOnlineBase";
/**
 * The Room class.
 */
export declare abstract class Room extends SceneOnlineBase {
    /**
     * The constructor for the Room class.
     * @param gameElement The element the game is running in.
     * @param socket The socket pointing to the server.
     */
    constructor(gameElement: HTMLDivElement, socket: SocketIOClient.Socket);
    protected __type(): SceneType;
    abstract readonly roomName: string;
    abstract preloadPlayers(data: any): void;
    abstract spoke(data: any): void;
    abstract joined(data: any): void;
    abstract left(data: any): void;
    abstract walked(data: any): void;
    abstract throw(data: any): void;
    abstract resize(): void;
}
