import { SceneType } from "./SceneType";
import { SceneOnlineBase } from "./SceneOnlineBase";
/**
 * The Room class.
 */
export class Room extends SceneOnlineBase {
    /**
     * The constructor for the Room class.
     * @param gameElement The element the game is running in.
     * @param socket The socket pointing to the server.
     */
    constructor(gameElement, socket) {
        super(gameElement, socket);
    }
    __type() {
        return SceneType.Room;
    }
}
