import { SceneType } from "./SceneType";
/**
 * The base scene element, has the most basic scene properties.
 */
export declare abstract class SceneBase {
    /**
     * The type of scene it is.
     */
    readonly type: SceneType;
    /**
     * The main element of the game
     */
    protected gameElement: HTMLDivElement;
    /**
     * All the timeout's in this scene.
     */
    protected timeouts: Array<number>;
    /**
     * All the interval's in this scene.
     */
    protected intervals: Array<number>;
    /**
     * Constructor for the SceneBase class.
     * @param gameElement The element the game is running in.
     */
    constructor(gameElement: HTMLDivElement);
    /**
     * This should return what type of scene it is.
     * @return What type of scene it is.
     */
    protected __type(): SceneType;
    /**
     * Sets up the scene.
     */
    abstract setup(): void;
    /**
     * Generates a element that covers the entire scene. Useful for sandboxing.
     * @return absolutely positioned div with all sides set to `0`
     */
    sceneElement(): HTMLDivElement;
    /**
     * Destory's the scene.
     *
     * Get's rid of any timeout's and interval's left in the scene.
     */
    destroy(): void;
}
