import { SceneType } from "./SceneType";
/**
 * The base scene element, has the most basic scene properties.
 */
export class SceneBase {
    /**
     * Constructor for the SceneBase class.
     * @param gameElement The element the game is running in.
     */
    constructor(gameElement) {
        this.gameElement = gameElement;
        this.timeouts = [];
        this.intervals = [];
        this.type = this.__type();
    }
    /**
     * This should return what type of scene it is.
     * @return What type of scene it is.
     */
    __type() {
        return SceneType.Base;
    }
    /**
     * Generates a element that covers the entire scene. Useful for sandboxing.
     * @return absolutely positioned div with all sides set to `0`
     */
    sceneElement() {
        let sceneElement = document.createElement("div");
        {
            sceneElement.style.position = "absolute";
            sceneElement.style.top = "0";
            sceneElement.style.left = "0";
            sceneElement.style.right = "0";
            sceneElement.style.bottom = "0";
        }
        return sceneElement;
    }
    /**
     * Destory's the scene.
     *
     * Get's rid of any timeout's and interval's left in the scene.
     */
    destroy() {
        let { timeouts, intervals } = this;
        for (let timeout of timeouts) {
            clearTimeout(timeout);
        }
        for (let interval of intervals) {
            clearInterval(interval);
        }
    }
}
