/// <reference types="socket.io-client" />
import { SceneType } from "./SceneType";
import { SceneBase } from "./SceneBase";
/**
 * OfflineMenu Class.
 * @param gameElement Where to display the menu.
 */
export declare abstract class SceneMenu extends SceneBase {
    /**
     * The constructor for the SceneMenu class.
     * @param gameElement The element the game is running in.
     */
    constructor(gameElement: HTMLDivElement);
    protected __type(): SceneType;
}
import { SceneOnlineBase } from "./SceneOnlineBase";
/**
 * SceneOnlineMenu Class.
 */
export declare abstract class SceneOnlineMenu extends SceneOnlineBase {
    /**
     * The constructor for the SceneOnlineMenu class.
     * @param gameElement The element the game is running in.
     * @param socket The socket pointing to the server.
     */
    constructor(gameElement: HTMLDivElement, socket: SocketIOClient.Socket);
    protected __type(): SceneType;
}
