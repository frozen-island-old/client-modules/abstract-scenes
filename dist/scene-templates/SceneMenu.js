import { SceneType } from "./SceneType";
import { SceneBase } from "./SceneBase";
/**
 * OfflineMenu Class.
 * @param gameElement Where to display the menu.
 */
export class SceneMenu extends SceneBase {
    /**
     * The constructor for the SceneMenu class.
     * @param gameElement The element the game is running in.
     */
    constructor(gameElement) {
        super(gameElement);
    }
    __type() {
        return SceneType.Menu;
    }
}
;
import { SceneOnlineBase } from "./SceneOnlineBase";
/**
 * SceneOnlineMenu Class.
 */
export class SceneOnlineMenu extends SceneOnlineBase {
    /**
     * The constructor for the SceneOnlineMenu class.
     * @param gameElement The element the game is running in.
     * @param socket The socket pointing to the server.
     */
    constructor(gameElement, socket) {
        super(gameElement, socket);
    }
    __type() {
        return SceneType.OnlineMenu;
    }
}
;
