import { SceneType } from "./SceneType";
import { SceneBase } from "./SceneBase";
/**
 * The base scene for things that need a socket to the server.
 */
export class SceneOnlineBase extends SceneBase {
    /**
     * The constructor for the SceneOnlineBase class.
     * @param gameElement The element the game is running in.
     * @param socket The socket pointing to the server.
     */
    constructor(gameElement, socket) {
        super(gameElement);
        this.socket = socket;
    }
    /**
     * Overrides the type of the scene.
     * @return That we are a BaseOnline type.
     */
    __type() {
        return SceneType.Online;
    }
}
