/**
 * The types of scenes avalable.
 */
export declare enum SceneType {
    Base = 0,
    Menu = 1,
    Online = 2,
    OnlineMenu = 3,
    Room = 4,
    Minigame = 5
}
