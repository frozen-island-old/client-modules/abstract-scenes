/**
 * The types of scenes avalable.
 */
export var SceneType;
(function (SceneType) {
    SceneType[SceneType["Base"] = 0] = "Base";
    SceneType[SceneType["Menu"] = 1] = "Menu";
    SceneType[SceneType["Online"] = 2] = "Online";
    SceneType[SceneType["OnlineMenu"] = 3] = "OnlineMenu";
    SceneType[SceneType["Room"] = 4] = "Room";
    SceneType[SceneType["Minigame"] = 5] = "Minigame";
})(SceneType || (SceneType = {}));
;
