import { SceneBase } from "./SceneBase";
/**
 * The base scene for things that need a socket to the server.
 */
export class Starter extends SceneBase {
    /**
     * The constructor for the SceneOnlineBase class.
     * @param gameElement The element the game is running in.
     */
    constructor(gameElement) {
        super(gameElement);
    }
}
