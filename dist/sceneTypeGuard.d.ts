import { SceneBase } from "./scene-templates/SceneBase";
import { Starter } from "./scene-templates/Starter";
import { SceneMenu, SceneOnlineMenu } from "./scene-templates/SceneMenu";
import { SceneOnlineBase } from "./scene-templates/SceneOnlineBase";
import { Room } from "./scene-templates/Room";
import { Minigame } from "./scene-templates/Minigame";
export declare function isOnline(scene: SceneBase | Starter | SceneMenu | SceneOnlineMenu | SceneOnlineBase | Room | Minigame): scene is SceneOnlineBase;
export declare function isRoom(scene: SceneBase | Starter | SceneMenu | SceneOnlineMenu | SceneOnlineBase | Room | Minigame): scene is Room;
