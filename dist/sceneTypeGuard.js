import { SceneType } from "./scene-templates/SceneType";
export function isOnline(scene) {
    switch (scene.type) {
        case SceneType.Online:
        case SceneType.OnlineMenu:
        case SceneType.Room:
        case SceneType.Minigame:
            return true;
        default:
            return false;
    }
}
export function isRoom(scene) {
    switch (scene.type) {
        case SceneType.Room:
            return true;
        default:
            return false;
    }
}
