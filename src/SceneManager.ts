import { SceneBase } from "./scene-templates/SceneBase";
import { Starter } from "./scene-templates/Starter";
import { SceneMenu } from "./scene-templates/SceneMenu";
import { Room } from "./scene-templates/Room";
import { Minigame } from "./scene-templates/Minigame";

/**
 * Provides scenes to the engine.
 */
export abstract class SceneManager {
    /**
     * Nothing should need to go here...
     */
    constructor() {}

    /**
     * The starting room, used to check if the global server is responding or to bootstrap connections.
     * @param gameElement The element the game is running in.
     * @return A scene.
     */
    public abstract starting(gameElement: HTMLDivElement): Starter;

    /**
     * Returns the loading element.
     * @param loadingElement where the loading element should be displayed.
     * @return [description]
     */
    public abstract loading(loadingElement: HTMLDivElement): SceneBase;

    /**
     * Returns a room for logging in & connecting to servers.
     * @param gameElement The element the game is running in.
     * @return The login / connect menu scene.
     */
    public abstract login(gameElement: HTMLDivElement): SceneMenu;

    /**
     * Returns the room the player just moved to.
     * @param roomName The room the player is moveing to.
     * @param gameElement The element the game is running in.
     * @param socket The socket pointing to the server.
     * @return A game room or minigame room.
     */
    public abstract room(roomName: string, gameElement: HTMLDivElement, socket: SocketIOClient.Socket): Room | Minigame;

    /**
     * Displays a error.
     * @param gameElement The element errors get displayed in.
     * @param errorNumber the error code, used for formatting the error data.
     * @param errorData the error data
     * @return The error to display.
     */
    public abstract error(gameElement: HTMLDivElement, errorNumber: number, errorData: string[]): SceneBase;

    /**
     * The scene displayed when connecting to a server.
     * @param gameElement The element the game is running in.
     * @return A room.
     */
    public abstract connecting(gameElement: HTMLDivElement): SceneBase;
}
