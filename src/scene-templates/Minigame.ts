import {SceneType} from "./SceneType";
import { SceneOnlineBase } from "./SceneOnlineBase";

/**
 * Minigame Class.
 */
export abstract class Minigame extends SceneOnlineBase {
    /**
     * The constructor for the Minigame class.
     * @param gameElement The element the game is running in.
     * @param socket The socket pointing to the server.
     */
    constructor(gameElement: HTMLDivElement, socket: SocketIOClient.Socket) {
        super(gameElement, socket);
    }
    protected __type(): SceneType {
        return SceneType.Minigame;
    }
};
