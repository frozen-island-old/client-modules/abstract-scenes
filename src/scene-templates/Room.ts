import {SceneType} from "./SceneType";
import {SceneOnlineBase} from "./SceneOnlineBase";

/**
 * The Room class.
 */
export abstract class Room extends SceneOnlineBase {
    /**
     * The constructor for the Room class.
     * @param gameElement The element the game is running in.
     * @param socket The socket pointing to the server.
     */
    constructor(gameElement: HTMLDivElement, socket: SocketIOClient.Socket) {
        super(gameElement, socket);
    }

    protected __type(): SceneType {
        return SceneType.Room;
    }

    public abstract readonly roomName: string;

    public abstract preloadPlayers(data: any): void;

    public abstract spoke(data: any): void;

    public abstract joined(data: any): void
    public abstract left(data: any): void;
    public abstract walked(data: any): void;
    public abstract throw(data: any): void;

    public abstract resize(): void;
}
