import {SceneType} from "./SceneType";
import {SceneBase} from "./SceneBase";
/**
 * OfflineMenu Class.
 * @param gameElement Where to display the menu.
 */
export abstract class SceneMenu extends SceneBase {
    /**
     * The constructor for the SceneMenu class.
     * @param gameElement The element the game is running in.
     */
    constructor(gameElement: HTMLDivElement){
        super(gameElement);
    }
    protected __type(): SceneType {
        return SceneType.Menu;
    }
};

import { SceneOnlineBase } from "./SceneOnlineBase";

/**
 * SceneOnlineMenu Class.
 */
export abstract class SceneOnlineMenu extends SceneOnlineBase {
    /**
     * The constructor for the SceneOnlineMenu class.
     * @param gameElement The element the game is running in.
     * @param socket The socket pointing to the server.
     */
    constructor(gameElement: HTMLDivElement, socket: SocketIOClient.Socket) {
        super(gameElement, socket);
    }
    protected __type(): SceneType {
        return SceneType.OnlineMenu;
    }
};
