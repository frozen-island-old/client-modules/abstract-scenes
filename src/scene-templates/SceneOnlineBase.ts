import {SceneType} from "./SceneType";
import {SceneBase} from "./SceneBase";
/**
 * The base scene for things that need a socket to the server.
 */
export abstract class SceneOnlineBase extends SceneBase {
    /**
     * The socket pointing to the server.
     */
    protected socket: SocketIOClient.Socket;

    /**
     * The constructor for the SceneOnlineBase class.
     * @param gameElement The element the game is running in.
     * @param socket The socket pointing to the server.
     */
    constructor(gameElement: HTMLDivElement, socket: SocketIOClient.Socket) {
        super(gameElement);
        this.socket = socket;
    }

    /**
     * Overrides the type of the scene.
     * @return That we are a BaseOnline type.
     */
    protected __type(): SceneType {
        return SceneType.Online;
    }
}
