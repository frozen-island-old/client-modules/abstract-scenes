/**
 * The types of scenes avalable.
 */
export enum SceneType {
    Base,
    Menu,
    Online,
    OnlineMenu,
    Room,
    Minigame,
};
