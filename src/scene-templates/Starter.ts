import {SceneBase} from "./SceneBase";
/**
 * The base scene for things that need a socket to the server.
 */
export abstract class Starter extends SceneBase {
    /**
     * The constructor for the SceneOnlineBase class.
     * @param gameElement The element the game is running in.
     */
    constructor(gameElement: HTMLDivElement) {
        super(gameElement);
    }

    /**
     * Returns if the master server is online.
     * @return True: Master server is online
     */
    abstract isOnline(): boolean;

    /**
     * Returns if the client should stop trying.
     * @return True: Client can't connect
     */
    abstract isDead(): boolean;
}
