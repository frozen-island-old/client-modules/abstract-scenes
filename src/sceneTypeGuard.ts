import { SceneBase } from "./scene-templates/SceneBase";
import { Starter } from "./scene-templates/Starter";
import { SceneMenu, SceneOnlineMenu } from "./scene-templates/SceneMenu";
import { SceneOnlineBase } from "./scene-templates/SceneOnlineBase";
import { Room } from "./scene-templates/Room";
import { Minigame } from "./scene-templates/Minigame";
import { SceneType } from "./scene-templates/SceneType";

export function isOnline(scene: SceneBase | Starter | SceneMenu | SceneOnlineMenu | SceneOnlineBase | Room | Minigame): scene is SceneOnlineBase {
    switch(scene.type){
        case SceneType.Online:
        case SceneType.OnlineMenu:
        case SceneType.Room:
        case SceneType.Minigame:
            return true;
        default:
            return false;
    }
}

export function isRoom(scene: SceneBase | Starter | SceneMenu | SceneOnlineMenu | SceneOnlineBase | Room | Minigame): scene is Room {
    switch(scene.type){
        case SceneType.Room:
            return true;
        default:
            return false;
    }
}
